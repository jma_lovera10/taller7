-Si es posible llegar al mismo resultado pues la relación que se tiene entre la posición del pre orden y las de pos orden es una jerarquía que se define de derecha a izquierda. En otras palabras, se revisa uno por uno los caracteres del pre orden y se ubican en el pos orden. Luego de tener esto se tiene que los elementos que se encuentran a la izquierda son aquellos que son hijos, exceptuando los sub grupos que ya se han definido de la misma forma anteriormente.

Ejemplo: d,e,b,f,g,c,a (pos)  a,b,d,e,x,f,g (pre) a->{c->{g,f},b{e,d}}

-Para el in orden y pos orden no es posible establecer una relación que de como resultado el árbol original pues sus datos se encuentran dispersos. No obstante, se puede tener una forma a partir de la cual se construye el árbol de abajo hacia arriba.

