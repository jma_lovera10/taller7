package taller;

public class ArbolBN<T extends Comparable<T>>{
	
	private Node<T> root;
	
	public ArbolBN() {
		root = null;
	}
	
	public void add(T[] inorder, T[] preorder){
		T elem = preorder[0];
		int i=0;
		while(i<inorder.length){
			if(elem.equals(inorder[i]))break;
			i++;
		}
		root = new Node<T>(elem);
		int[] pos = new int[1];
		pos[0] = 1;
		root.add(i,inorder.length,pos,inorder,preorder);
	}
	
	public class Node<T extends Comparable<T>> {
		
		Node<T> left;
		Node<T> right;
		T elem;
		public Node(T elem) {
			left = null;
			right = null;
			this.elem = elem;
		}
		public void add(int mid, int top, int[] pos, T[] inorder, T[] preorder) {
			if(mid==top||pos[0]>=preorder.length)return;
			T elem = preorder[pos[0]];
			int i = 0;
			while(i<mid){
				if(elem.equals(inorder[i])){
					left = new Node<T>(elem);
					pos[0]++;
					left.add(i,mid, pos, inorder, preorder);
					break;
				}
				i++;
			}
			i = mid+1;
			if(pos[0]>=preorder.length)return;
			elem = preorder[pos[0]];
			while(i<top){
				if(elem.equals(inorder[i])){
					right = new Node<T>(elem);
					pos[0]++;
					right.add(i,top, pos, inorder, preorder);
					break;
				}
				i++;
			}
		}
		
	}
}
