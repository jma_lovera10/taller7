package taller.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;
import taller.ArbolBN;

public class MainTest extends TestCase{

	public void testSample() {
		String[] testPreorden1 = {"A","B","D","E","C","F","G"};
		String[] testInorden1 = {"D","B","E","A","F","C","G"};
		String[] testPreorden2 = {"J","K","L","O","Q","H","P"};
		String[] testInorden2 = {"K","L","J","H","Q","O","P"};
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String res1 = "";
		String res2 = "";
		
		try{
			BufferedReader bf = new BufferedReader(new FileReader(new File("data/testResp1.txt")));
			res1 = bf.readLine();
			while(bf.ready()){
				res1+="\n"+bf.readLine();
			}
			bf.close();
			bf = new BufferedReader(new FileReader(new File("data/testResp2.txt")));
			res2 = bf.readLine();
			while(bf.ready()){
				res2+="\n"+bf.readLine();
			}
			bf.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		ArbolBN<String> arbol1 = new ArbolBN<String>();
		ArbolBN<String> arbol2 = new ArbolBN<String>();
		
		arbol1.add(testInorden1, testPreorden1);
		arbol2.add(testInorden2, testPreorden2);
		
		String json = gson.toJson(arbol1);
		String json1 = gson.toJson(arbol2);
		
		assertEquals("La organización del árbol no es la esperada",res1, json);
		assertEquals("La organización del árbol no es la esperada",res2, json1);
	}

}