package taller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class QueueFIFO {

	public static String decode(String a,String b,int carry,int base,String[] set){
        int res = 0;
        int op1 = -1;
        int op2 = -1;
        int i = 0;
        while(op1==-1&&op2==-1){
            if(op1==-1)op1= set[i].equals(a)?i:op1;
            if(op2==-1)op2= set[i].equals(b)?i:op2;
            i++;
        }
        res = op1+op2;
        carry = res/base;
        res %= base;
        return ""+res+","+carry;
    }
    
    public static void main(String[] args) {
        try{
            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
            String data = bf.readLine();
            System.out.println(data);
            String[] dataArr = data.split(" ");
            int base = Integer.parseInt(dataArr[0]);
            String[] set = dataArr[1].split("");
            String op1 = bf.readLine();
            System.out.println(op1);
            String op2 = bf.readLine();
            System.out.println(op2);
            String ep = bf.readLine();
            System.out.println(ep);
            String[] operator1 = op1.trim().split("");
            String[] operator2 = op2.trim().split("");
            
            int i = operator1.length-1;
            int j = operator2.length-2;
            String a;
            String b;
            int carry = 0;
            String[] values;
            String res = "";
            while(true){
                if(carry==0&&i<0&&j<0)break;
                if(carry==0&&(i<0||j<0)){
                    if(i<0)res= operator2[j--]+res;
                    if(j<0)res= operator1[i--]+res;
                    continue;
                } 
                if(i<0){
                    a = set[0];
                }else{
                    a = operator1[i--];
                }
                if(j<0){
                    b = set[0];
                }else{
                    b = operator2[j--];
                }
                values = decode(a,b,carry,base,set).split(",");
                res = values[0]+res;
                carry = Integer.parseInt(values[1]);
            }
            System.out.println(res);
        }catch(IOException io){
            io.printStackTrace();
        }
    }
}
